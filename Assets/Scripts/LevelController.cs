﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
    public static LevelController instance = null;

    // You really have to set this one.
    public int levelID = 0;

    // These are auto-fetched by the script
    public int score = 0;
    private float startingTime;
    public float floatTimeValue = 0;

	// Use this for initialization
	void Start () {
        if (instance != null)
        {
            Destroy(this);
            return;
        }
        else
        {
            instance = this;
            try
            {
                GameObject go = GameObject.Find("TimeLabel");
                TimerController tc = go.GetComponent<TimerController>();
                startingTime = tc.startTime;
            }
            catch
            {
                startingTime = Time.time;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        GameObject go = GameObject.Find("Player");
        PlayerController pc = go.GetComponent<PlayerController>();
        score = pc.score;

        go = GameObject.Find("TimeLabel");
        TimerController tc = go.GetComponent<TimerController>();
        floatTimeValue = tc.floatTimeValue;
    }
}
