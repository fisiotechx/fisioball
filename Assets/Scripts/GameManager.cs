﻿using Assets.Scripts.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public DatabaseController databaseController = null;
    public UserDataController userDataController = null;
    public bool ready = false;

    [SerializeField]
    public int DebugUserDataID = 0;
    [SerializeField]
    public string DebugUserDataName = "";

    public static bool isReady ()
    {
        return (instance != null);
    }

    private void Start()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
            instance.ready = true;
            DontDestroyOnLoad(target: this);
            instance.Setup(redefine: false);
        }
    }

    private void Update()
    {
        instance.DebugData();
    }

    private void DebugData()
    {
        if (userDataController != null)
        {
            if (userDataController.userData != null)
            {
                DebugUserDataID = userDataController.userData.user_id;
                DebugUserDataName = userDataController.userData.name;
            }
        }
        if (instance == null)
        {
            Debug.LogError("Where is the fucking game manager?");
        }
    }

    private bool Setup(bool redefine)
    {
        instance.databaseController = new DatabaseController();
        instance.userDataController = new UserDataController();
        User u = null;
        if (redefine)
        {
            instance.databaseController.RedefineDatabase();
            instance.databaseController.InsertUser(user_id: 1, user_name: "Jogador");
        }
        u = instance.databaseController.GetUser(user_id: 1);
        Debug.Log("Got User: " + u.user_id + "(" + u.user_id + ").");
        u.levelDataList = instance.databaseController.GetLevelDataList(user_id: u.user_id);
        Debug.Log("Got Level Data list. List has " + u.levelDataList.Count + " elements.");

        userDataController.userData = u;
        Debug.Log("Setup complete.");
        return (u != null);
    }

    public void RedefineDatabase()
    {
        bool result = instance.Setup(redefine: true);
        if (!result)
        {
            Debug.LogError("ERROR AT DATABASE DEFINITION");
        }
    }
}
