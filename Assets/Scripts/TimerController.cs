﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour {

    public Text timerText;
    public float startTime;
    private bool finished = false;

    public float floatTimeValue = 0;
    public int h, m;
    public float s;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
	}

    // Update is called once per frame
    void Update()
    {
        floatTimeValue = Time.time - startTime;
        h = (int) floatTimeValue / 60 / 60;
        m = (int) (floatTimeValue / 60) % 60;
        s = floatTimeValue % 60;

        string hours = h.ToString("f0").PadLeft(2, '0');
        string minutes = m.ToString("f0").PadLeft(2, '0');
        string seconds = s.ToString("f2").PadLeft(5, '0');
        timerText.text = hours + ":" + minutes + ":" + seconds;
    }
}
