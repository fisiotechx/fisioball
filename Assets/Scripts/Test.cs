﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class Test : MonoBehaviour
{
    Rigidbody rb;


    SerialPort stream = new SerialPort("COM3", 250000); //Set the port (com4) and the baud rate (9600, is standard on most devices)
    float[] lastRot = { 0, 0 }; //Need the last rotation to tell how far to spin the camera
    Vector3 rot;
    Vector3 offset;

    public int HorizontalMultiplier = 1;
    public int VerticalMultiplier = 1;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        stream.Write("1");
        string value = stream.ReadLine();
        string[] vec3 = value.Split(';');
        if (vec3[0] != "" && vec3[1] != "")
        {
            float newX = float.Parse(vec3[0]) * HorizontalMultiplier;
            float newY = float.Parse(vec3[1]) * VerticalMultiplier;
            rot = new Vector3(newX, 0.0f, newY);
            // Debug.Log("DATA...: " + rot.x + ", " + rot.y);
            stream.BaseStream.Flush();
        }

        rb.AddForce(rot * 5);
        
    }

    void OpenSerialPort()
    {
        stream.Open(); //Open the Serial Stream.
    }

    void CloseSerialPort()
    {
        stream.Close();
    }

    void OnGUI()
    {

        //  GUI.Label(new Rect(10, 10, 300, 100), newString); 
        GUI.Label(new Rect(10, 30, 300, 100), "\t" + rot);
    }
}