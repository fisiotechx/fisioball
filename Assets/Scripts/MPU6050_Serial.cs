﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

using System.Text;
using System;
using UnityEngine.UI;

public class MPU6050_Serial : MonoBehaviour
{
    

    public bool ready;

    //used for calibration
    Quaternion rotOffset = Quaternion.identity;
    bool calibrated;

    [Tooltip("If you are using Arduino Nano lower try lowe baud rates, don't forget to change it on Arduino file too")]
    public int baudRate = 115200;

    //Port that the arduino is using
    public string portCOM = "";

    //serial port used througout the script
    SerialPort port;

    //interval for sending something to the arduino
    float interval;

    //variable retrieved from the arduino
    public Quaternion q = Quaternion.identity;

    //bytes received from arduino
    public byte[] bytes;

    //number of times data was read
    public int count = 0;

    //used to convert sensor coordinate system to unity's coodinate system
    float angle;
    Vector3 axis;

    // Use this for initialization
    void Start()
    {

        //set to have 14*2=28
        bytes = new byte[28];

        //automatically find port
        if (portCOM == "" && SerialPort.GetPortNames().Length > 0)
        {
            portCOM = SerialPort.GetPortNames()[0];
            Debug.Log(portCOM);
        }
        try
        {
            //set up port
            port = new SerialPort(portCOM, baudRate);

            //set buffer size
            port.ReadBufferSize = 500;

            //open port
            port.Open();

            //set newline characters
            port.NewLine = "\r\n";

            //set timeout
            port.ReadTimeout = 20;

            //send r to the arduino
            port.Write("r");

        }
        //in case something goes wrong
        catch (System.Exception e)
        {
            Debug.LogWarning(e.Message + "\nArduino might not be connected.\nAlso Make that you don't have the Arduino app's Serial Monitor opened.");
        }

    }

    // Update is called once per frame
    void Update()
    {


        if (port.IsOpen)
        {
            //If W is pressed, calibrate and start rotation changing
            if (Input.GetKeyDown(KeyCode.W) && ready && calibrated == false)
            {
                CalibrationStep(q);
            }

            //resend r just in case
            if (Time.time - interval > 1)
            {
                // resend single character to trigger DMP init/start
                // in case the MPU is halted/reset while applet is running
                port.Write("r");
                interval = Time.time;

            }



            try
            {
                //get 28 bytes from buffer
                if (port.Read(bytes, 0, 28) == 28)
                    count++;

                //flush for safety
                port.BaseStream.Flush();


                //set the quaternion
                //<<8 means shifting left 8 bits and | mean concatenate 2 bytes
                //1638 is the sensitivity o the accelerometer inside the MPU6050
                q.x = ((bytes[2] << 8) | bytes[3]) / 16384.0f;
                q.y = ((bytes[4] << 8) | bytes[5]) / 16384.0f;
                q.z = ((bytes[6] << 8) | bytes[7]) / 16384.0f;
                q.w = ((bytes[8] << 8) | bytes[9]) / 16384.0f;

                //fix quaternions
                for (int i = 0; i < 4; i++) if (q[i] >= 2) q[i] = -4 + q[i];

                //transform sensor's coordinate system into unity's coordinate sytem
                q.ToAngleAxis(out angle, out axis);
                q = Quaternion.AngleAxis(angle, new Vector3(axis[2], axis[0], -axis[1]));

                if (bytes[0] == 36 && bytes[1] == 2)
                    ready = true;

                if (calibrated)
                {
                    //like this it rotates locally, if it is  q*rotOffset it will rotate in worldspace
                    transform.rotation = rotOffset * q;
                }


            }
            //in case something goes wrong
            catch (System.Exception)
            {

            }
        }
    }

    private void LateUpdate()
    {
    }

    //close port on quit
    void OnApplicationQuit()
    {
        if (port != null && port.IsOpen == true)
            port.Close();
    }


    //find the relation between unity's coordiante system and sensors coordinate system
    public void CalibrationStep(Quaternion sensorQuat)
    {
        rotOffset = QuatSubtraction(transform.rotation, sensorQuat);
        calibrated = true;
    }


    //the equivalent to a subtraction for quaternions
    Quaternion QuatSubtraction(Quaternion a, Quaternion b)
    {
        return a * Quaternion.Inverse(b);
    }

}
