﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayMusicOnStart : MonoBehaviour {

    [SerializeField]
    public AudioSource audioSource_reference;
    public AudioClip audiofile;

    private bool isAudioExecuting = false;

	// Use this for initialization
	void Awake () {
        if (isAudioExecuting)
        {
            audioSource_reference.Stop();
        }
        else
        {
            try
            {
                audioSource_reference.clip = audiofile;
                audioSource_reference.Play();
                isAudioExecuting = true;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
