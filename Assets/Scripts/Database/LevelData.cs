﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Database
{
    public class LevelData
    {
        public int level_id;
        public int score;
        public float time;
        public string date;

        public LevelData(int level_id, int score, float time, string date)
        {
            this.level_id = level_id;
            this.score = score;
            this.time = time;
            this.date = date;
        }
    }
}
