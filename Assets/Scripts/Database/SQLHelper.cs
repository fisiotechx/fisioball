﻿using Mono.Data.SqliteClient;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class SQLHelper {
    public static IDataReader SendSQLCommand (string sql, bool resultable)
    {
        Debug.Log("[SQL HELPER]: " + "Creating connection...");
        IDbConnection connection = new SqliteConnection(DatabaseController.getDatabaseURL());
        Debug.Log("[SQL HELPER]: " + "Creating command...");
        IDbCommand command = connection.CreateCommand();
        command.CommandText = sql;
        connection.Open();
        if (resultable)
        {
            Debug.Log("[SQL HELPER]: " + "Sending resultable command: " + sql);
            IDataReader idr = null;
            idr = command.ExecuteReader();
            Debug.Log("[SQL HELPER]: " + "Returned IDR Data: "  + idr.RecordsAffected + " affected. " + "Depth: " + idr.Depth + ", FieldCount: " + idr.FieldCount);
            return idr;
        }
        else
        {
            Debug.Log("[SQL HELPER]: " + "Sending not resultable command: " + sql);
            command.ExecuteNonQuery();
            return null;
        }
    }
}
