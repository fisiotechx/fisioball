﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Database
{
    public class User
    {
        public int user_id;
        public string name;
        public List<LevelData> levelDataList;
        public User(int user_id, string name)
        {
            this.user_id = user_id;
            this.name = name;
            this.levelDataList = new List<LevelData>();
        }
    }
}
