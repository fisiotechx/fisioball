﻿using Assets.Scripts.Database;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class DatabaseController {
    public static string DatabaseName = "fisioball_database.fdb";

    public static string getDatabaseURL()
    {
        return "URI=file:" + DatabaseName;
    }

    public void CreateDataTables()
    {
        string sql_command;

        // Create User table
        sql_command = "CREATE TABLE IF NOT EXISTS USERS (user_id INTEGER NOT NULL, name VARCHAR(100) NOT NULL);";
        SQLHelper.SendSQLCommand(sql_command, false);
        // Create LevelData table
        sql_command = "CREATE TABLE IF NOT EXISTS LEVELDATA (user_id INTEGER, level_id INTEGER, score INTEGER, time REAL, date VARCHAR(100));";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public void DropDataTables()
    {
        string sql_command;
        // Create User table
        sql_command = "DROP TABLE IF EXISTS USERS;";
        SQLHelper.SendSQLCommand(sql_command, false);
        // Create LevelData table
        sql_command = "DROP TABLE IF EXISTS LEVELDATA;";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public User GetUser(int user_id)
    {
        User user = null;

        string sql_command;
        sql_command = "SELECT * FROM USERS WHERE user_id = " + user_id + ";";
        IDataReader idr = SQLHelper.SendSQLCommand(sql_command, true);
        if (idr != null)
        {
            if (idr.Read())
            {
                Debug.Log("Got: " + idr.GetInt32(0));
                Debug.Log("Got: " + idr.GetString(1));
                int n_uid = idr.GetInt32(0);
                string n_uname = idr.GetString(1);
                user = new User(n_uid, n_uname);
                idr.Close();
            }
        }
        else
        {
            Debug.LogError("Fatal error at Database Access!");
        }
       
        return user;
    }

    public void InsertUser(User user)
    {
        string sql_command;
        sql_command = "INSERT INTO USERS VALUES (" + user.user_id + ", '" + user.name + "');";
        SQLHelper.SendSQLCommand(sql_command, false);

        foreach (LevelData levelData in user.levelDataList)
        {
            sql_command = "INSERT INTO LEVELDATA VALUES (" + user.user_id + ", " + levelData.level_id + ", " + levelData.score + ", " + levelData.time + ");";
            SQLHelper.SendSQLCommand(sql_command, false);
        }
    }

    public void InsertUser(int user_id, string user_name)
    {
        string sql_command;
        sql_command = "INSERT INTO USERS VALUES (" + user_id + ", '" + user_name+ "');";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public void UpdateUser(User user)
    {
        if (GetUser(user.user_id) != null)
        {
            string sql_command;
            sql_command = "UPDATE USERS SET name = '" + user.name + "' WHERE user_id = " + user.user_id + ";";
            SQLHelper.SendSQLCommand(sql_command, false);
        }
        else
        {
            InsertUser(user);
        }
    }

    public void DeleteUser(int user_id)
    {
        User user;
        user = GetUser(user_id);

        foreach (LevelData levelData in user.levelDataList)
        {
            DeleteLevelData(user_id, levelData);
        }

        string sql_command;
        sql_command = "DELETE FROM USERS WHERE user_id = " + user_id + ";";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public LevelData GetLevelData(int user_id, int level_id)
    {
        LevelData levelData = null;

        string sql_command;
        sql_command = "SELECT * FROM LEVELDATA WHERE user_id = " + user_id + ", level_id = " + level_id + ";";
        IDataReader idr = SQLHelper.SendSQLCommand(sql_command, true);

        if (idr.Read())
        {
            int n_lid = idr.GetInt32(1);
            int n_lscore = idr.GetInt32(2);
            float n_ltime = idr.GetFloat(3);
            string n_ldate = idr.GetString(4);

            levelData = new LevelData(n_lid, n_lscore, n_ltime, n_ldate);
            idr.Close();
        }

        return levelData;
    }

    public void InsertLevelData(User u, LevelData levelData)
    {
        string sql_command;
        sql_command = "INSERT INTO LEVELDATA VALUES (" + u.user_id + ", " + levelData.level_id + ", " + levelData.score + ", " + levelData.time + ", '" + levelData.date + "');";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public void InsertLevelData(int user_id, LevelData levelData)
    {
        string sql_command;
        sql_command = "INSERT INTO LEVELDATA VALUES (" + user_id + ", " + levelData.level_id + ", " + levelData.score + ", " + levelData.time + ", '" + levelData.date + "');";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public void InsertLevelData(int user_id, int level_id, int score, float time, string date)
    {
        string sql_command;
        sql_command = "INSERT INTO LEVELDATA VALUES (" + user_id + ", " + level_id + ", " + score + ", " + time + ", '" + date + "');";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public void UpdateLevelData(int user_id, LevelData levelData)
    {
        if (GetLevelData(user_id, levelData.level_id) != null)
        {
            string sql_command;
            sql_command = "INSERT INTO LEVELDATA VALUES (" + user_id + ", " + levelData.level_id + ", " + levelData.score + ", " + levelData.time + ", '" + levelData.date + "');";
            SQLHelper.SendSQLCommand(sql_command, false);
        }
        else
        {
            InsertLevelData(user_id, levelData);
        }
    }

    public void UpdateLevelData(int user_id, int level_id, int score, float time, string date)
    {
        if (GetLevelData(user_id, level_id) != null)
        {
            string sql_command;
            sql_command = "INSERT INTO LEVELDATA VALUES (" + user_id + ", " + level_id + ", " + score + ", " + time + ", '" + date + "');";
            SQLHelper.SendSQLCommand(sql_command, false);
        }
        else
        {
            InsertLevelData(user_id, level_id, score, time, date);
        }
    }

    public void DeleteLevelData(int user_id, LevelData levelData)
    {
        string sql_command;
        sql_command = "DELETE FROM LEVELDATA WHERE user_id = " + user_id + ", level_id = " + levelData.level_id + ";";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public void DeleteLevelData(int user_id, int level_id)
    {
        string sql_command;
        sql_command = "DELETE FROM LEVELDATA WHERE user_id = " + user_id + ", level_id = " + level_id + ";";
        SQLHelper.SendSQLCommand(sql_command, false);
    }

    public List<LevelData> GetLevelDataList(int user_id)
    {
        LevelData levelData = null;
        List<LevelData> levelDataList = new List<LevelData>();

        string sql_command;
        sql_command = "SELECT * FROM LEVELDATA WHERE user_id = " + user_id + ";";
        IDataReader idr = SQLHelper.SendSQLCommand(sql_command, true);

        while (idr.Read())
        {
            int n_lid = idr.GetInt32(1);
            int n_lscore = idr.GetInt32(2);
            float n_ltime = idr.GetFloat(3);
            string n_ldate = idr.GetString(4);

            Debug.Log("Read data from result set: " + n_lid + ", " + n_lscore + ", " + n_ltime + ", " + n_ldate); 
            levelData = new LevelData(n_lid, n_lscore, n_ltime, n_ldate);
            levelDataList.Add(levelData);
            // idr.NextResult();
        }
        idr.Close();

        return levelDataList;
    }

    public void RedefineDatabase()
    {
        Debug.LogWarning("[" + this.GetType().Name + "]: " + "Trying to redefine game data.");
        DropDataTables();
        CreateDataTables();
        Debug.Log("[" + this.GetType().Name + "]: " + "Game data has been redefined successfully.");
    }
}
