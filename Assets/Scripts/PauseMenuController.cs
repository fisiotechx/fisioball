﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public bool isGamePaused = false;
    public string ExitSceneName = "Menu_ZoneSelect";
    public GameObject pauseMenuObject;
    public AudioClip pauseSound;
    public AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        if (audioSource != null)
        {
            audioSource.clip = pauseSound;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isGamePaused)
            {
                audioSource.Play();
                Unpause();
            }
            else
            {
                audioSource.Play();
                pauseMenuObject.SetActive(true);
                Time.timeScale = 0f;
                isGamePaused = true;
            }
        } else if (Input.GetKeyDown(KeyCode.S))
        {
            if (isGamePaused)
            {
                audioSource.Play();
                Unpause();
                SceneManager.LoadScene(ExitSceneName);
            }
        }
    }

    public void Unpause()
    {
        pauseMenuObject.SetActive(false);
        Time.timeScale = 1f;
        isGamePaused = false;
    }

    public void loadScene(string name)
    {
        if (isGamePaused)
        {
            Unpause();
        }
    }
}
