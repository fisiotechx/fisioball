﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopCameraController : MonoBehaviour {

    public Vector3 position_offset;
    public Vector3 rotation_offset;


    // Use this for initialization
    void Start () {
        gameObject.transform.Rotate(rotation_offset);
    }

    void FixedUpdate()
    {
        Vector3 newPosition = GameObject.Find("Player").transform.position + position_offset;
        //gameObject.transform.rotation.Set(rotation_offset.x, rotation_offset.y, rotation_offset.z, gameObject.transform.rotation.w);
        gameObject.transform.position = newPosition;
        //gameObject.transform.rotation.eulerAngles.Set(rotation_offset.x, rotation_offset.y, rotation_offset.z);
        //gameObject.transform.Rotate(rotation_offset);
        Quaternion newQuaternion = new Quaternion(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z, gameObject.transform.rotation.w);
        //Debug.Log("X: " + gameObject.transform.rotation.x + ", Y: " + gameObject.transform.rotation.y + ", Z: " + gameObject.transform.rotation.z + ", W: " + gameObject.transform.rotation.w);
    }

    // Update is called once per frame
    void Update () {
        /*Vector3 newPosition = GameObject.Find("Player").transform.position + offset;
        gameObject.transform.position = newPosition;*/
     }
}
