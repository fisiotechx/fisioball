﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {
    [SerializeField]
    public string sceneName = "";

    public void gotoScene(string sn)
    {
        SceneManager.LoadScene(sn);
    }

    private void OnMouseUp()
    {
        gotoScene(sceneName);
    }
}
