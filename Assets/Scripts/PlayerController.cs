﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Text;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;

    public System.DateTime initial_date;
    Time lastMoved;

    public float SpeedMultiplier = 10f;
    public float Threshold = 1f;

    public int score = 0;
    public int neededScore = 10;

    public float finishmarkCounter = 0f;
    public float finishmarkThreshold = 0.01f;

    public Text scoreLabel;
    public Text timeLabel;

    public GameObject AccelerometerReference;

    public AudioSource audioSource;
    public AudioClip coinSound;

    public float Y_AXIS_DeathPlanePosition = -10;


    private void Start()
    {
        initial_date = System.DateTime.Now.ToLocalTime();
        rb = gameObject.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (gameObject.transform.position.y < Y_AXIS_DeathPlanePosition)
        {
            Lose();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verificar colisão com Collectible
        if (other.gameObject.CompareTag("Collectible"))
        {
            other.gameObject.SetActive(false);
            GameObject.Destroy(other.gameObject);
            score += 1;
            scoreLabel.text = score + " pts / " + neededScore + " pts";
            if (score >= neededScore)
            {
                Win();
            }
            audioSource.clip = coinSound;
            audioSource.Play();
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        // Verificar colisão com Hazard
        if (collision.collider.gameObject.CompareTag("Hazard"))
        {
            Lose();
        }

        //
        if (collision.collider.gameObject.CompareTag("Finishmark"))
        {
            if (finishmarkCounter > 1)
            {
                finishmarkCounter = 0;
                Win();
            }
            else
            {
                finishmarkCounter += 1 * finishmarkThreshold;
            }
        }
    }

    private void FixedUpdate()
    {
        /*
            Vector3 newMovement = new Vector3(AccelerometerReference.gameObject.transform.rotation.y, 0, AccelerometerReference.gameObject.transform.rotation.x);
        rigidbody.AddForce(newMovement * SpeedMultiplier * Threshold);
        */
        float h_sp = Input.GetAxis("Horizontal");
        float v_sp = Input.GetAxis("Vertical");
        Vector3 input_force = new Vector3(h_sp, 0, v_sp);
        rb.AddForce(input_force * SpeedMultiplier * Threshold);
    }

    private void Lose()
    {
        score = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SaveLevelData()
    {
        if (GameManager.isReady())
        {
            GameManager.instance.databaseController.InsertLevelData(user_id: GameManager.instance.userDataController.userData.user_id, level_id: 1, score: score, time: LevelController.instance.floatTimeValue, date: initial_date.ToShortDateString() + " " + initial_date.ToShortTimeString());
        }
        else
        {
            Debug.LogError("Data not sent.");
        }
    }

    private void Win()
    {
        SaveLevelData();
        score = 0;
        SceneManager.LoadScene("Menu_ZoneSelect");
    }
}