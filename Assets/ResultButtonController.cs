﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultButtonController : MonoBehaviour {
    public int level_id;
    public string date;
    public int session_number;
    public float time;
    public int score;

    public Text session_number_text = null;
    public Text date_text = null;
    public Text level_id_text = null;
    public Text time_text = null;
    public Text score_text = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Set(int session_number, int level_id, float time, int score, string date)
    {
        this.session_number = session_number;
        this.level_id = level_id;
        this.date = date;
        this.time = time;
        this.score = score;

        session_number_text.text = "Sessão #" + session_number;
        level_id_text.text = "Fase 1-" + level_id;
        date_text.text = date;
        time_text.text = time.ToString();
        score_text.text = "" + score + " pts";

        Debug.Log("Set Data:" + session_number + ", " + level_id + ", " + time + ", " + score + ", " + date);
    }
}
