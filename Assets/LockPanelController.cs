﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPanelController : MonoBehaviour {
    public GameObject LockBackgroundPanelObject;
    public GameObject ConfirmRedefineDialogObject;
    public GameObject ConnectionToServerDialogObject;
    public bool activated = false;

    // Use this for initialization
    void Start () {
        if (activated)
        {
            ShowLockBackground();
        } else
        {
            HideLockBackground();
        }
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void ShowLockBackground()
    {
        LockBackgroundPanelObject.SetActive(true);
        activated = true;
    }

    public void HideLockBackground()
    {
        LockBackgroundPanelObject.SetActive(false);
        activated = false;
    }

    public void Activate_ConfirmRedefineDialog()
    {
        ShowLockBackground();
        ConfirmRedefineDialogObject.SetActive(true);
    }

    public void Deactivate_ConfirmRedefineDialog()
    {
        ConfirmRedefineDialogObject.SetActive(false);
        HideLockBackground();
    }

    public void Activate_ConnectionToServerDialog()
    {
        ShowLockBackground();
        ConnectionToServerDialogObject.SetActive(true);
    }

    public void Deactivate_ConnectionToServerDialog()
    {
        ConnectionToServerDialogObject.SetActive(false);
        HideLockBackground();
    }
}
