﻿using Assets.Scripts.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultController : MonoBehaviour {
    public UnityEngine.UI.Text pacient_name_label;
    public UnityEngine.UI.Text pacient_code_label;

    public GameObject buttonReferencePrefab = null;

    public GameObject notReadyObject = null;

    public GameObject sessionListObject = null;

    private bool inserted = false;

    // Use this for initialization
    void Start () {
		if (GameManager.isReady())
        {
            pacient_code_label.text = "Código: " + GameManager.instance.userDataController.userData.user_id;
            pacient_name_label.text = GameManager.instance.userDataController.userData.name;
            int i = 0;
            foreach (LevelData levelData in GameManager.instance.userDataController.userData.levelDataList)
            {
                i++;
                Debug.Log("Inserting Data:" + i + ", " + levelData.level_id + ", " + levelData.time + ", " + levelData.score + ", " + levelData.date);
                GameObject button = Instantiate(buttonReferencePrefab);
                button.transform.SetParent(sessionListObject.transform);
                ResultButtonController rbc = button.GetComponent<ResultButtonController>();
                rbc.Set(i, levelData.level_id, levelData.time, levelData.score, levelData.date);
                Debug.Log("Inserted result");
            }

            inserted = true;
        }
        else
        {
            ShowUnavailableData();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ShowUnavailableData()
    {
        notReadyObject.SetActive(true);
        Debug.LogError("Data not ready.");
    }
}
